"""
 Copyright 2019 Holly Hoppet
 Distributed under the MIT software license, see the accompanying
 file COPYING or http://www.opensource.org/licenses/mit-license.php.
"""

import pytest
import vigemclient
import random
import time
import ctypes
from option import Result
from vigemclient import Client, TargetX360, XUSBReport, XUSBButton

@pytest.fixture
def client_and_x360_target() -> (Client, TargetX360):
    client = vigemclient.Client()
    target = vigemclient.TargetX360()
    client.add_target(target)

    return (client, target)

def test_add_target(client_and_x360_target):
    client, target = client_and_x360_target

    assert target in client.get_targets()

def test_update(client_and_x360_target):
    _, target = client_and_x360_target
    
    report = XUSBReport()
    report.buttons = XUSBButton.a | XUSBButton.b
    target.update(report)

def test_get_playernum(client_and_x360_target):
    _, target = client_and_x360_target

    playernum: Result = target.get_playernum()
    assert playernum.is_ok

def test_get_playernum_error():
    errorTarget = vigemclient.TargetX360()
    
    playernum: Result = errorTarget.get_playernum()
    assert playernum.is_err

'''
This test should be run on its own. You can watch it do its job by opening up the windows controller control panel. It
will send a random set of inputs every three seconds indefinitely

It is advisable to not have steam running while executing this test because it does funny things when it sees the guide
button being pressed
'''
@pytest.mark.skip(reason="Test doesn't end. Run on its own")
def test_update_indefinitely(client_and_x360_target):
    target = client_and_x360_target[1]

    while(True):
        # Send a random set of inputs every three seconds
        report = XUSBReport()

        # First set a random set of buttons
        buttons_list = [
                XUSBButton.up, XUSBButton.down, XUSBButton.left, XUSBButton.right, XUSBButton.start, XUSBButton.back,
                XUSBButton.left_thumb, XUSBButton.right_thumb, XUSBButton.left_shoulder, XUSBButton.right_shoulder,
                XUSBButton.guide, XUSBButton.a, XUSBButton.b, XUSBButton.x, XUSBButton.y
                ]
        report.buttons = random.choice(buttons_list)
        for _ in range(8):
            report.buttons |= random.choice(buttons_list)

        # Randomly assign values for our analog components
        trigger_range = range(256)
        thumb_range = range(-32768, 32768)
        
        report.left_trigger = random.choice(trigger_range)
        report.right_trigger = random.choice(trigger_range)
        report.thumb_left_x = random.choice(thumb_range)
        report.thumb_left_y = random.choice(thumb_range)
        report.thumb_right_x = random.choice(thumb_range)
        report.thumb_right_y = random.choice(thumb_range)

        # Send the report then sleep for three seconds before the next set of input
        target.update(report)
        time.sleep(2)