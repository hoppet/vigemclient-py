/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

// Windows imports
#include <windows.h>

#include "ViGEm/Client.h"
#include "cpp_wrapping/client.h"
#include "cpp_wrapping/target_x360.h"

namespace py = pybind11;

PYBIND11_MODULE(vigemclient, m) {
    py::class_<Client, std::shared_ptr<Client>>(m, "Client")
        .def(py::init<>())
        .def("add_target", &Client::add_target, py::arg("target"))
        .def("get_targets", &Client::get_targets);

    py::class_<Target, std::shared_ptr<Target>>(m, "Target")
        .def_readwrite("parent", &Target::parent);

    py::class_<TargetX360, Target, std::shared_ptr<TargetX360>>(m, "TargetX360")
        .def(py::init<>())
        .def("update", &TargetX360::update, py::arg("report"))
        .def("get_playernum", &TargetX360::get_playernum);

    py::class_<XUSB_REPORT>(m, "XUSBReport")
        .def(py::init<>())
        //TODO: Maybe it would be nice to abstract away the bitmasking of wButtons in C++ so it doesn't have to be done
        //      in python
        //TODO: Describe value limits for analog inputs
        .def_readwrite("buttons", &XUSB_REPORT::wButtons)
        .def_readwrite("left_trigger", &XUSB_REPORT::bLeftTrigger)
        .def_readwrite("right_trigger", &XUSB_REPORT::bRightTrigger)
        .def_readwrite("thumb_left_x", &XUSB_REPORT::sThumbLX)
        .def_readwrite("thumb_left_y", &XUSB_REPORT::sThumbLY)
        .def_readwrite("thumb_right_x", &XUSB_REPORT::sThumbRX)
        .def_readwrite("thumb_right_y", &XUSB_REPORT::sThumbRY);
        
    py::enum_<VIGEM_ERROR>(m, "Error")
        .value("none", VIGEM_ERROR_NONE)
        .value("bus_not_found", VIGEM_ERROR_BUS_NOT_FOUND)
        .value("no_free_slot", VIGEM_ERROR_NO_FREE_SLOT)
        .value("invalid_target", VIGEM_ERROR_INVALID_TARGET)
        .value("removal_failed", VIGEM_ERROR_REMOVAL_FAILED)
        .value("already_connected", VIGEM_ERROR_ALREADY_CONNECTED)
        .value("target_uninitialized", VIGEM_ERROR_TARGET_UNINITIALIZED)
        .value("bus_version_mismatch", VIGEM_ERROR_BUS_VERSION_MISMATCH)
        .value("bus_access_failed", VIGEM_ERROR_BUS_ACCESS_FAILED)
        .value("callback_already_registered", VIGEM_ERROR_CALLBACK_ALREADY_REGISTERED)
        .value("callback_not_found", VIGEM_ERROR_CALLBACK_NOT_FOUND)
        .value("bus_already_connected", VIGEM_ERROR_BUS_ALREADY_CONNECTED)
        .value("bus_invalid_handle", VIGEM_ERROR_BUS_INVALID_HANDLE)
        .value("xusb_userindex_out_of_range", VIGEM_ERROR_XUSB_USERINDEX_OUT_OF_RANGE)
        .export_values();

    py::enum_<XUSB_BUTTON>(m, "XUSBButton", py::arithmetic())
        .value("up", XUSB_GAMEPAD_DPAD_UP)
        .value("down", XUSB_GAMEPAD_DPAD_DOWN)
        .value("left", XUSB_GAMEPAD_DPAD_LEFT)
        .value("right", XUSB_GAMEPAD_DPAD_RIGHT)
        .value("start", XUSB_GAMEPAD_START)
        .value("back", XUSB_GAMEPAD_BACK)
        .value("left_thumb", XUSB_GAMEPAD_LEFT_THUMB)
        .value("right_thumb", XUSB_GAMEPAD_RIGHT_THUMB)
        .value("left_shoulder", XUSB_GAMEPAD_LEFT_SHOULDER)
        .value("right_shoulder", XUSB_GAMEPAD_RIGHT_SHOULDER)
        .value("guide", XUSB_GAMEPAD_GUIDE)
        .value("a", XUSB_GAMEPAD_A)
        .value("b", XUSB_GAMEPAD_B)
        .value("x", XUSB_GAMEPAD_X)
        .value("y", XUSB_GAMEPAD_Y)
        .export_values();

    m.attr("__version__") = "dev";
}
