"""
 Copyright 2019 Holly Hoppet
 Distributed under the MIT software license, see the accompanying
 file COPYING or http://www.opensource.org/licenses/mit-license.php.
"""

import sys
from distutils.core import setup
from distutils.extension import Extension

import setuptools
from setuptools.command.build_ext import build_ext
import pybind11

ext_modules = [
    Extension('vigemclient',
              [
                  "ViGEmClient/src/ViGEmClient.cpp",
                  "vigemclient_py.cpp",
                  "cpp_wrapping/client.cpp",
                  "cpp_wrapping/target.cpp",
                  "cpp_wrapping/target_x360.cpp"
              ],
              language='c++',
              include_dirs=[
                  pybind11.get_include(True),
                  pybind11.get_include(False),
                  "ViGEmClient/include"],
                  libraries=["Setupapi"]
              # library_dirs=["ViGEmClient/lib/release/x86"],
              # libraries=["ViGEmClient"]
              )
]

setup(
    name='vigemclient-py',
    version='0.1',
    description='Python bindings for ViGEmClient via pybind11',
    author='hollyhoppet',
    author_email='hollyscsc@gmail.com',
    maintainer='hollyhoppet',
    maintainer_email='hollyscsc@gmail.com',
    package_dir={'vigemclient': './*'},
    classifiers=[
        'Operating System :: Microsoft :: Windows'
        'Programming Language :: Python :: 3.7'
    ],
    install_requires=[
        'pybind11==2.2.4',
        'pytest==4.4.1'
                      ],
    zip_safe=False,
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules
)
