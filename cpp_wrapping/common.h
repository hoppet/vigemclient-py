/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#pragma once

// Windows SDK min/max conflict with pybind11's defined min/max, but thankfully windows lets us disable theirs.
#define NOMINMAX

#include <windows.h>
#include "ViGEm/Client.h"
