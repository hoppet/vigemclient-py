/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#include "client.h"
#include "target.h"

Client::Client() {
    backing_client = vigem_alloc();
    VIGEM_ERROR error = vigem_connect(backing_client);
    if(!VIGEM_SUCCESS(error)) {
        //TODO: Throw an exception class instead
        throw error;
    }
}

Client::~Client() {
    vigem_disconnect(backing_client);
    vigem_free(backing_client);
}

PVIGEM_CLIENT Client::get_backing_client() {
    return this->backing_client;
}

VIGEM_ERROR Client::add_target(std::shared_ptr<Target> target) {
    VIGEM_ERROR error = vigem_target_add(this->get_backing_client(), target->get_backing_target());

    if(VIGEM_SUCCESS(error)) {
        this->targets->push_back(target);
        target->parent = shared_from_this();
    }

    return error;
}

Client::target_vector* Client::get_targets() {
    return this->targets.get();
}