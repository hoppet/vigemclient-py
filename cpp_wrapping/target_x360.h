/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#pragma once

#include "target.h"
#include <pybind11/pybind11.h>

namespace py = pybind11;

class TargetX360: public Target {
    public:
        TargetX360();
        ~TargetX360();
        
        VIGEM_ERROR update(XUSB_REPORT report);
        py::object get_playernum();
};
