/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#pragma once
#include <memory>
#include "common.h"

class Client;

class Target: public std::enable_shared_from_this<Target> {
public:
    virtual ~Target();
    std::weak_ptr<Client> parent;

    PVIGEM_TARGET get_backing_target();
protected:
    PVIGEM_TARGET backing_target;
};