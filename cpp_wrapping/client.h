/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#pragma once

#include <memory>
#include <vector>
#include "common.h"

class Target;


class Client: public std::enable_shared_from_this<Client> {
public:
    typedef std::vector<std::shared_ptr<Target>> target_vector;

    Client();
    ~Client();

    /*
    TODO: I would much prefer this return shared_ptr<target_vector>, but pybind11 seems to not like that a lot. This
          should avoid the double-free problem mentioned in the pybind11 documentation on smart pointers because Target
          inherits enable_shared_from_this, but I'd still like to research why it chokes on a smart pointer to a vector
          of smart pointers and possibly raise a bug. This will be after the first release of JoyConvey is done
     */
    target_vector* get_targets();

    PVIGEM_CLIENT get_backing_client();
    VIGEM_ERROR add_target(std::shared_ptr<Target> target);
private:
    PVIGEM_CLIENT backing_client;

    // A vector containing all of of the Targets associated with this Client 
    std::shared_ptr<target_vector> targets = std::make_shared<target_vector>();
};