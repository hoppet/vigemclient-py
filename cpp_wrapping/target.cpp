/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#include "target.h"

Target::~Target() {}

PVIGEM_TARGET Target::get_backing_target() {
    return this->backing_target;
}