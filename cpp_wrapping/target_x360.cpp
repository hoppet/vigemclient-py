/* 
 *  Copyright 2019 Holly Hoppet
 *  Distributed under the MIT software license, see the accompanying
 *  file COPYING or http://www.opensource.org/licenses/mit-license.php.
 */

#include "target_x360.h"
#include "client.h"

TargetX360::TargetX360() {
    this->backing_target = vigem_target_x360_alloc();
}

TargetX360::~TargetX360() {
    vigem_target_free(this->backing_target);
}

VIGEM_ERROR TargetX360::update(XUSB_REPORT report) {
    if(auto p = this->parent.lock()) {
        return vigem_target_x360_update(p->get_backing_client(), this->backing_target, report);
    } else {
        //TODO: maybe return a more specific error to this case.. which maybe means somehow extending the struct? Can
        //      you even do that in C++ lol? Or I'd have to wrap the error enum which i'm not too keen on.
        return VIGEM_ERROR_INVALID_TARGET;
    }
}

py::object TargetX360::get_playernum() {
    VIGEM_ERROR error;
    PULONG playernum;

    if(auto p = this->parent.lock()) {
        error = vigem_target_x360_get_user_index(p->get_backing_client(), this->backing_target, playernum);
    } else {
        error = VIGEM_ERROR_INVALID_TARGET;
    }

    // Import the option module so we can use Result.
    py::object option = py::module::import("option");
    py::object result = option.attr("Result");

    // If there's no error, create a (python) Result.Ok with the player number. Otherwise a Result.Err with the error
    // code. Dang pybind11 is wild lol.
    if(error == VIGEM_ERROR_NONE) {
        py::object ok = result.attr("Ok");
        return ok(playernum);
    } else {
        py::object err = result.attr("Err");
        return err(error);
    }
    
}
